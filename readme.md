## Description
A docker image to be used with Bibbucket pipelines for building Gradle based
 project builds and pushing them to an AWS ECR repo. 

## Building
The image is hosted on bitbucket and, at the moment, is built through a 
 DockerHub "automated build", which pulls the code from bitbucket and does 
 a docker build/push to the repo.  I might change this to be based on a 
 BitBucket pipeline instead, because I dislike the way DockerHub requires 
 manual matching of the version tag to the image tag. 
 I think I'd rather drive the DockerHub image from the dockerPush
 task which is based of the "version.x.x.x" git tag the way I like it.
